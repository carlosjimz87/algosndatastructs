function diagonalDifference(arr) {
    // Write your code here
    var N = arr.length
    var i = N

    var diff = arr.reduce((diff, a) => {
        if(i>0)i--;
        return diff + a[i]-a[N-1-i]
    },0)

    return Math.abs(diff)
    
}

var arr = [[11, 2, 4],[4, 5, 6], [10, 8, -12]];
// arr = [[1, 2, 3],[4, 5, 6], [9, 8, 9]];

a = diagonalDifference(arr)
console.log("Diff:",a)
