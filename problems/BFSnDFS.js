function DFS() {}

const airports = "PHK BKK OKC JFK LAX MEX EZE HEL LOS LAP LIM".split(" ");

const routes = [
  ["PHK", "LAX"],
  ["PHK", "JFK"],
  ["JFK", "OKC"],
  ["JFK", "HEL"],
  ["JFK", "LOS"],
  ["MEX", "LAX"],
  ["MEX", "BKK"],
  ["MEX", "LIM"],
  ["MEX", "EZE"],
  ["LIM", "BKK"],
];

//the graph
const adjacencyList = new Map();

//add nodes
function addNode(airport) {
  adjacencyList.set(airport, []);
}

//add edges (undierected, no cycles)
function addEdge(origin, destination) {
  adjacencyList.get(origin).push(destination);
  adjacencyList.get(destination).push(origin);
}

//create the graph
airports.forEach(addNode);
routes.forEach((route) => addEdge(...route));

console.log(adjacencyList);

function bfs(startingAirport, criteria) {
  // create a queue (FIFO) with first airport (node)
  const queue = [startingAirport];
  // create a set to list visited airports (only once ocurrence is allowed)
  const visited = new Set();
  // iterate the queue while not empty
  while (queue.length > 0) {
    // get the first airport of the queue (removing it from the queue)
    const airport = queue.shift();
    // get all destinations of that airport
    const destinations = adjacencyList.get(airport);
    //iterate all destinations of that airport
    for (const destination of destinations) {
      // if destination matches criteria we print the output
      if (destination === criteria) {
        console.log("Found it : ", airport, "=>", destination);
      }

      // after found the node that matches the criteria,
      // we add it in the queue as is a possible new airport to search in,
      // but also we need to add it in the set to avoid re-visiting (if not: infinit cycle error)
      if (!visited.has(destination)) {
        visited.add(destination);
        queue.push(destination);
        console.log(destination);
      }
    }
  }
}

function dfs(airport, criteria, visited = new Set()) {
  // the first airport is already added to the set and this way we avoid to re-enter the same function for this airport (if not: infinit function calls)
  visited.add(airport);
  console.log(airport);
  // we get all destinations for that airport
  const destinations = adjacencyList.get(airport);

  //iterating all the destinations
  for (const destination of destinations) {
    // if destination matches the criteria, we print the output and leave the function (last case of recursivity)
    if (destination === criteria) {
      console.log(`Found it : ${airport} => ${destination}`);
      return;
    }

    // if the node (destination) it hasen't visited yet,
    // we re-call the same function with the new destination as airport and we start all over again
    if (!visited.has(destination)) {
      dfs(destination, criteria, visited);
    }
  }
}

// bfs("PHK","BKK")
dfs("PHK", "BKK");
