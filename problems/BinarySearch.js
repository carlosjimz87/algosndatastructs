// recursive
var binarySearchRecursive = function (arr, x, left, right) {
  if (left > right) return false;
  arr = arr.sort();
  var mid = (left + right) >> 1;

  if (arr[mid] === x) {
    return true;
  } else if (x < arr[mid]) {
    return binarySearchRecursive(arr, x, left, mid - 1);
  } else {
    return binarySearchRecursive(arr, x, mid + 1, right);
  }
};

//iterative
var binarySearchIterative = function (arr, x) {
  var left = 0,
    right = arr.length - 1;
  arr = arr.sort();
  while (left <= right) {
    var mid = (left + right) >> 1;

    if (arr[mid] === x) {
      return true;
    } else if (x < arr[mid]) {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
  }
  return false;
};

var findValwBS = function (arr, x) {
  //   return binarySearchRecursive(arr, x, 0, arr.length - 1);
  return binarySearchIterative(arr, x);
};

var arr = [13, 54, 67, 23, 14, 67, 34, 76, 23, 84, 23, 45, 67];
var x = 84;

let r = findValwBS(arr, x);

console.log(r);
